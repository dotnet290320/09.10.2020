﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexerDemo
{
    class Team
    {
        private List<Employee> m_members;
        private List<string> m_resp;

        // cheat - boost performance
        // private Dictionary<string, Employee> map_name_to_employee = new Dictionary<string, Employee>();

        public Team(List<string> resp)
        {
            m_members = new List<Employee>();
            m_resp = resp;
        }

        public Employee this[string name]
        {
            get
            {
                Employee result = m_members.FirstOrDefault(e => e.Name.ToUpper() == name.ToUpper());
                return result;

                // cheat for faster performance
                // map_name_to_employee.TryGetValue(name, out Employee emp);
                //return emp;
            }
        }

        // indexer [ int ] -- first option id
        //public Employee this[int id]
        //{
        //    get
        //    {
        //        Employee result = m_members.FirstOrDefault(e => e.Id == id);
        //        return result;
        //    }
        //}

        // indexer [ int ] -- second option index
        public Employee this[int index]
        {
            get
            {
                if (index < 0 || index >= m_members.Count)
                    return null;

                Employee result = m_members[index];
                return result;
            }

            internal set // (int index, Employee value)
            {
                m_members[index] = value;
            }
        }

        //cannot auto generate -- error!
        //public Employee this[double d] { get; set; }

        public Employee GetEmployeeByIndex(int index)
        {

            if (index < 0 || index >= m_members.Count)
                return null;

            Employee result = m_members[index];
            return result;
        }

        public Employee GetEmployeeByName(string name)
        {
            //Employee result = null;
            //foreach (Employee e in m_members)
            //{
            //    if (e.Name.ToUpper() == name.ToUpper())
            //    {
            //        result = e;
            //        break;
            //    }
            //}

            //return m_members.FirstOrDefault(e => e.Name.ToUpper() == name.ToUpper());

            Employee result = m_members.FirstOrDefault(e => e.Name.ToUpper() == name.ToUpper());
            return result;

        }

        public void AddToTeam(Employee e)
        {
            if (e == null)
                throw new ArgumentException($"trying to add null employee to team {this}");
            m_members.Add(e);
            //map_name_to_employee.Add(e.Name, e);
        }


    }
}
