﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexerDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee dan = new Employee() { Id = 347563, Name = "Dan", Salary = 40000 };
            Employee ran = new Employee() { Id = 283746, Name = "Ran", Salary = 30000 };
            Employee avi = new Employee() { Id = 345984, Name = "Avi", Salary = 20000 };

            Team dream_team = new Team(new List<string>() { "Development client", "Server" });
            dream_team.AddToTeam(dan);
            dream_team.AddToTeam(ran);
            dream_team.AddToTeam(avi);

            // foreach over my class
            //foreach (Employee emp in dream_team)
            //{
            //    Console.WriteLine(emp);
            //}

            int[] arr = { 1, 2, 3 };
            Console.WriteLine(arr[0]);

            Dictionary<string, string> map_name_address = new Dictionary<string, string>();
            map_name_address.Add("haim", "ashdod");
            Console.WriteLine(map_name_address["haim"]);

            Dictionary<Employee, string> map_emp_obj_address = new Dictionary<Employee, string>();
            map_emp_obj_address.Add(avi, "ashdod");
            Console.WriteLine(map_emp_obj_address[avi]);

            //Console.WriteLine(dream_team["Dan"]);
            Console.WriteLine(dream_team.GetEmployeeByName("Dan"));
            Console.WriteLine($"with indexer name=dan:  {dream_team["Dan"]}");
            //Console.WriteLine($"with indexer id=283746: {dream_team[283746]}");
            Console.WriteLine($"with indexer position=1: {dream_team[1]}");
            Console.WriteLine($"with indexer position=1: {dream_team.GetEmployeeByIndex(1)}");

            dream_team[0] = new Employee() { Id = 394857, Name = "sagit", Salary = 35000 };
            Console.WriteLine($"dream 47: {dream_team[0]}");


        }
    }
}
